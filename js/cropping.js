/**
 * Created by Benno on 08.01.17.
 */

var saveToFile = function () {
};

function rawSaveToFile() {
	console.log("Savgin to...");
	saveToFile();
}

function bindCropping() {
	console.log("loading cropping...");
	var dragging = false;

	var clickPosition = {x: 0, y: 0};
	var cropper = $('#cropper');
	var toCrop = $('#to-crop');

	var initialSize = {width: toCrop.width(), height: toCrop.height()};
	var positionBeforeDrag = {x: toCrop.position().left, y: toCrop.position().top};

	var zoomFactor = 1;

	var $window = $(window);

	cropper.on('mousedown', function (event) {
		event.preventDefault();
		dragging = true;
		clickPosition = {x: event.pageX, y: event.pageY};
		positionBeforeDrag = {x: toCrop.position().left, y: toCrop.position().top};
	});


	$window.on('mousemove', function (event) {
		if (dragging) {
			toCrop.css({left: positionBeforeDrag.x - (clickPosition.x - event.pageX) + "px", top: positionBeforeDrag.y - (clickPosition.y - event.pageY) + "px"});
		}
	});

	$window.on('mouseup', function () {
		dragging = false;
	});
	
	// http://stackoverflow.com/a/17283938/
	cropper.on('mousewheel DOMMouseScroll', function (event) {
		event.preventDefault();

		var delta;

		if (event.originalEvent.wheelDelta !== undefined)
			delta = event.originalEvent.wheelDelta;
		else if (event.originalEvent.deltaY !== undefined)
			delta = event.originalEvent.deltaY * -1;
		else
			delta = -event.originalEvent.detail;

		var currentWidth = initialSize.width * zoomFactor;

		console.log("zoomfactor: " + zoomFactor);
		console.log("delta: " + delta);

		if (delta > 0) {
			zoomFactor *= 1.1;
		} else {
			zoomFactor *= 0.9;
		}

		console.log("new zoom factor: " + zoomFactor);

		var newWidth = initialSize.width * zoomFactor;
		var addition = (currentWidth - newWidth) / 3.75;
		var modifier = (addition < 0 ? "-" : "+") + "=" + Math.abs(addition);

		toCrop.css({width: newWidth, left: modifier, top: modifier});

	});


	saveToFile = function () {
		console.log("SAVIGNGNGè");
		console.log("Post data:");
		console.log({
				action: 'crop-n-set',
				filename: toCrop.attr('src'),

				offsetX: -toCrop.position().left / zoomFactor,
				offsetY: -toCrop.position().top / zoomFactor,
				width: cropper.width() / zoomFactor,
				height: cropper.height() / zoomFactor
			});
		$.post('/api/profile-pic.php',
			{
				action: 'crop-n-set',
				filename: toCrop.attr('src'),

				offsetX: -toCrop.position().left / zoomFactor,
				offsetY: -toCrop.position().top / zoomFactor,
				width: cropper.width() / zoomFactor,
				height: cropper.height() / zoomFactor
			},
			function (data, status) {
				console.log(data);
				console.log("Success!");
				window.location.reload();
			});
	};
}

function unbindCropping() {
	var $window = $(window);
	var cropper = $('#cropper');

	cropper.off('mousedown mousewheel DOMMouseScroll');
	$window.off('mousemove mouseup');
}