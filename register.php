<?php

/*

<input type="text" id="username" name="username" placeholder="Benutzername" required>

                    <input type="text" name="displayname" placeholder="Anzeigename" required>

                    <input type="text" id="birthday" name="birthday" placeholder="Geburtstag (dd.mm.yyyy)" required>

                    <select name="gender">
                        <option value="2" selected>Keine Angabe</option>
                        <option value="0">Männlich</option>
                        <option value="1">Weiblich</option>
                    </select>
                    <input type="password" name="password" placeholder="Passwort" required>

 */

if (isset($_POST['username'])) {
	$_POST['register'] = "";
	include_once('user_management.php');

	$statement = $db->prepare("INSERT INTO users (username, displayname, password) VALUES (?, ?, ?)");
	$statement->bind_param("sss", $_POST['username'], $_POST['displayname'], hashPassword($_POST['password']));
	if (!$statement->execute()) {
		echo $db->error;
	}

	$birthday = DateTime::createFromFormat('d.m.Y', $_POST['birthday']);

	$statement = $db->prepare("INSERT INTO profiles (id, birthday, gender) VALUES (LAST_INSERT_ID(), ?, ?)");
	$statement->bind_param("si", $birthday->format('Y-m-d'), $_POST['gender']);
	if (!$statement->execute()) {
		echo $db->error;
	}

	header("Location: $_SERVER[APP_URL]/?register=");
}