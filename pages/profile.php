<?php

$username = $data;

if (!$theUser = getUserInformation($username)) {
	?>
    <div class="column">
		<?php echo $data; ?>
    </div>
    <div class="column">
        <h1>Dieses Profil existiert nicht.</h1></div>
	<?php
	die();
}

$isOwnProfile = $theUser['id'] == $_USER['id'];

?>

<div class="column">
    <div class="box">
        <div class="row">
            <div class="left">
                <img id="profile-pic" src="<?php echo $theUser['picture']; ?>">
            </div>
            <div class="right">
				<?php
				if ($isOwnProfile) {
					echo "<span><a id=\"displayname\">$theUser[displayname]</a><i id=\"edit-displayname\" class=\"fa fa-edit\"></i><br><small>@$theUser[username]</small></span>";
				} else {
					echo "<span>$theUser[displayname]<br><small>@$theUser[username]</small></span>";
				}
				?>
            </div>
        </div>
		<?php

		$gender = array(
			"Männlich",
			"Weiblich",
			"Keine Angabe"
		);

		$theUser['gender'] = $gender[$theUser['gender']];

		$data = array(
			"birthday" => "birthday-cake",
			"gender" => "user",
			"description" => "user-secret"
		);

		?>
        <div class="row" id="profile-details">
            <table>
                <tbody>
				<?php
				foreach ($data as $obj => $icon) {
					$value = empty($theUser[$obj]) ? "Keine Angabe" : $theUser[$obj];
					echo "<tr><td><i class=\"fa fa-$icon\" aria-hidden=\"true\"></i></td><td>$value</td>";
					if ($isOwnProfile && $obj == "description")
						echo "<td><i id=\"edit-description\" class=\"fa fa-edit\"></i></td>";
					echo "</tr>";
				}
				?>
                </tbody>
            </table>
        </div>
		<?php
		$stats = getUserStats($username);
		echo <<<HTML
        <div class="row stats">
            <div>$stats[posts]<br><span>Posts</span></div>
            <div>$stats[following]<br><span>Follower</span></div>
            <div>$stats[followers]<br><span>Folgt</span></div>
        </div>
HTML;

		if ($LOGGED_IN && !$isOwnProfile) {
			$is_followed_by = ($stats['is_followed_by'] ? "Folgt dir" : "Folgt dir nicht");
			$follows = ($stats['follows'] ? "Du folgst" : "Du folgst nicht");
			echo <<<HTML
            <div class="row follow">
                <div class="left">
                    <div class="follow-status">$is_followed_by</div><br>
                    <div class="follow-status">$follows</div>
                </div>
                <div class="right">
                    <div class="follow-button" data-follows="$stats[follows]">
                        <span id="button-follow" class="follow-button-text">Folgen</span>
                        <span id="button-unfollow" class="follow-button-text">Entfolgen</span>
                    </div>
                </div>
            </div>
HTML;

		}
		?>
    </div>
</div>

<div class="column">
    <div class="box" id="posts">

		<?php
		$posts = getPostsFrom($username);
		if (count($posts) == 0) {
			echo "Keine Beiträge gefunden!";
		} else {
			foreach ($posts as $post) {
				echo $post->html;
			}
		}
		?>

    </div>
</div>

<div id="image-preview">
    <img src="<?php echo "/$theUser[picture_big]"; ?>">
	<?php if ($isOwnProfile) { ?>
        <div id="upload-button">
            <i class="fa fa-upload" aria-hidden="true"></i>
        </div>

		<?php
	}

	?>
    <form class="hidden">
        <input type="file" name="file" id="profile-chooser" accept="image/*">
        <input type="hidden" name="action" value="upload">
    </form>
</div>

<div id="cropping" style="display: none">
	<div id="cropper">
		<img id="to-crop" src="/img/dummy_user_big.png">
	</div>
	<small>Scrollen zum Zoomen, Klicken zum verschieben</small>
	
	<div>Speichern <i class="fa fa-check" aria-hidden="true"></i></div>
</div>

<script type="text/javascript">
	$(window).on('scriptJsLoaded', function () {
			var oldestPost = <?php echo count($posts) > 0 ? $posts[count($posts) - 1]->timestamp : 0; ?>;

			registerInfiniteScroll(oldestPost, $("#posts"), {
				from: '<?php echo $username; ?>'
			});
		}
	);
	
	var userId = <?php echo $theUser['id']; ?>;
	
	$(function () {
		$.getScript('/js/profile.js');
		<?php
		if ($isOwnProfile) {
			?>  
			$.getScript('/js/cropping.js');
			<?php
		}
		?>
	});
</script>