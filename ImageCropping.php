<?php

class ImageCropping {

	/**
	 * Crops part of file to 512 x 512 Pixel.
	 * @param $filename String The source file
	 * @param $offsetX Int The offset in X direction
	 * @param $offsetY Int The offset in Y direction
	 * @param $width Int The target parts width
	 * @param $height Int The target parts height
	 * @return bool|array
	 */
	static function cropImage($filename, $offsetX, $offsetY, $width, $height) {
		list($imgWidth, $imgHeight, $imgType) = getimagesize($filename);

		if ($width != $height) return false;

		$pasteOffsetX = 0;
		$pasteOffsetY = 0;

		$originalOffsetX = abs($offsetX);
		$originalOffsetY = abs($offsetY);

		if ($offsetX < 0) {
			$pasteOffsetX = $originalOffsetX;
			$offsetX = 0;
		}

		if ($offsetY < 0) {
			$pasteOffsetY = $originalOffsetY;
			$offsetY = 0;
		}

		switch ($imgType) {
			case 1: // GIF
				$src_img = imagecreatefromgif($filename);
				break;
			case 2: // JPEG
				$src_img = imagecreatefromjpeg($filename);
				break;
			case 3: // PNG
				$src_img = imagecreatefrompng($filename);
				break;
		}

		if (!isset($src_img)) {
			return false;
		}

		$img = imagecreatetruecolor($width + 2 * $originalOffsetX, $height + 2 * $originalOffsetY);
		$white = imagecolorallocate($img, 255, 255, 255);

		imagefill($img, 0, 0, $white);
		imagecopy($img, $src_img, $pasteOffsetX, $pasteOffsetY, 0, 0, $imgWidth, $imgHeight);

		$result = array();

		// As imagecrop is broken (https://bugs.php.net/bug.php?id=67447)
		foreach (array(512, 256) as $size) {
			$result_big = imagecreatetruecolor($size, $size);
			imagefill($result_big, 0, 0, $white);
			imagecopyresized($result_big, $img, 0, 0, $offsetX, $offsetY, $size, $size, $width, $height);

			for ($i = 0; file_exists($output = $_SERVER['DOCUMENT_ROOT'] . "/tmp/tmp-" . $i . ".png"); $i++) ;

			imagepng($result_big, $output);
			imagedestroy($result_big);

			$result[$size == 512 ? 'big' : 'small'] = $output;
		}

		imagedestroy($img);
		imagedestroy($src_img);


		return $result;
	}

}