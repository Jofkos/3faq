/*    ---   TOP POPUP   ---    */
var topPopup = $("#top-popup");

$("#status").click(function (event) {
	topPopup.toggleClass("hidden");
	lockEvent(event);
});

topPopup.click(function (event) {
	event.stopPropagation();
});

$(window).click(function () {
	topPopup.addClass("hidden");
});

$("#logout").on('click', function () {
	setLoading(true);
	$.post(
		'/api/logout.php',
		{},
		function() {
			setLoading(false);
			window.location.reload();
		}
	)
});

$(".imgcontainer img").click(gallery);

$("#register").submit(function (event) {

	var $this = $(this).parent();
	var birthday = $this.find('#birthday');

	if (!/^\d{1,2}\.\d{1,2}\.\d{4}$/.test(birthday.val())) {
		birthday.html('mismatch');
		birthday[0].setCustomValidity("Ungültiges Datumformat");
		alert("Ungültiges Datumformat!");

		event.preventDefault();
	}

});


var addImages = $("#add-images");
var choosenPictures = $("#choosen-pictures");

console.log(addImages);

if (addImages.length) {
	var input = addImages.parent().parent().find("input[type='file']");
	var parent = addImages.parent();

	input.change(function() {
		choosenPictures.find('a, br').remove();

		$.each(input[0].files, function(index, value) {
			choosenPictures.append($('<a>' + value.name + '</a><br>'));
		});

	});

	addImages.click(function (event) {
		event.stopPropagation();
		event.preventDefault();

		input.click();
	});
}

/*    ---   LIKE and PROFILE links   ---    */
$(".post .right > span, .post .footer > i, .imgcontainer img").click(lockEvent);

/*    ---   LOADING ANIMATION TOGGLE   ---    */
var title = $("#title");
function setLoading(loading) {
	if (loading) {
		title.addClass("loading");
	} else if (title.hasClass('loading')) {
		var rotating = title.find("span:first-child");
		rotating.on('animationiteration webkitAnimationIteration', function () {
			rotating.off('animationiteration webkitAnimationIteration');
			title.removeClass("loading");
		});
	}
}

/*    ---   INLINE LIKE FUNCTION   ---    */
function like(button, postId) {
	if ($('#top-popup').find('form').length) return;
	var theButton = $(button);

	function likeToggle() {
		if (theButton.hasClass("fa-heart-o")) {
			theButton.removeClass("fa-heart-o");
			theButton.addClass("fa-heart");
		} else {
			theButton.removeClass("fa-heart");
			theButton.addClass("fa-heart-o");
		}
		theButton.html(" " + (parseInt(theButton.html().trim()) + (theButton.hasClass("fa-heart-o") ? -1 : 1)));
	}

	likeToggle(theButton);
	$.post(
		'/api/like.php',
		{
			like: theButton.hasClass("fa-heart"),
			id: postId
		},
		function (data, status) {
			if (data == "false") {
				likeToggle(theButton);
			}
		}
	);
}

/*    ---   INLINE LINK FUNCTION   ---    */
function goTo(target) {
	window.location=target;
}


function registerInfiniteScroll(oldestPost, postContainer, postData) {
	var running = false;
	$(window).on('scrollReachedBottom', function () {
		if (running) return;
		running = true;
		setLoading(true);

		$.post(
			'/api/posts.json',
			$.extend({}, postData, {
				olderThan: oldestPost
			}),
			function (data) {
				console.log(data);
				$.each(data, function (index, value) {
					var row = $(value.html);
					row.find(".right > span, .footer > i, .imgcontainer img").click(lockEvent);
					row.find(".imgcontainer img").click(gallery);

					row.appendTo(postContainer);
					oldestPost = value.timestamp;
				});
				setLoading(false);
				running = false;
			},
			"json"
		);
	});
}

$(window).scroll(function () {
	var win = $(window);
	var doc = $(document);
	
	if (win.scrollTop() + win.height() >= doc.height() - 10) {
		win.trigger("scrollReachedBottom");
		console.log("scrollReachedBottom");
	}
});

function lockEvent(event) {
	event.stopPropagation();
	event.preventDefault();
}


$(window).trigger("scriptJsLoaded");