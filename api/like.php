<?php
header("Content-Type: text/plain");

include_once("../post_management.php");
if (!$LOGGED_IN) die("false");

if (!isset($_POST['id']) || !is_numeric($_POST['id']) || !isset($_POST['like'])) {
	die("Missing value.");
}

if ($_POST['like'] == "true") {
	echo($db->query("INSERT INTO likes (`user`, `post`) VALUES ($_USER[id], $_POST[id])") ? "true" : "false");
} else {
	echo($db->query("DELETE FROM likes WHERE user = $_USER[id] AND post = $_POST[id]") ? "true" : "false");
}


echo $db->error;