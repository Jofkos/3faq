/**
 * Created by Benno on 12.01.17.
 */

var modal = $("#overlay");
var modalClick = $("#overlay-click");

var hideModal;

function showOverlay(toShow, startIndex, hideFunc) {
	var content = [].concat(toShow);
	var index = startIndex || 0;

	var currentContent = showOverlayFrame(content[index], -1);
	modal.show();

	hideModal = function() {
		$(document).off('keydown');
		modalClick.off('click');
		hideOverlayFrame(currentContent, -1);
		modal.hide();

		if (hideFunc !== undefined) {
			hideFunc();
		}

		hideModal = undefined;
	};

	function showOverlayFrame(obj, direction) {
		if (!obj.is("div.overlay-content")) {
			obj = $("<div></div>")
				.addClass("overlay-content")
				.append(obj);
		}

		obj.addClass("fade-in")
			.removeClass("left")
			.removeClass("right");
		if (direction !== -1)
			obj.addClass(direction ? "left" : "right");

		modal.append(obj);

		return obj;
	}

	function hideOverlayFrame(element, direction) { // right = true
		element.addClass("fade-out")
			.removeClass("left")
			.removeClass("right");

		if (direction !== -1)
			element.addClass(direction ? "left" : "right");

		setTimeout(function () {
			element.remove();
		}, 500);
	}

	modalClick.on('click', hideModal);



	$(document).on('keydown', function(e) {
		var oldIndex = index;
		switch(e.which) {
			case 37: // left
				if (index === 0) {
					index = content.length;
				}

				index--;
				break;
			case 39: // right
				index = ++index % content.length;
				break;
			case 27: // escape
				hideModal();
				return;
			default: return;
		}

		if (index !== oldIndex) {
			hideOverlayFrame(currentContent, e.which == 39);
			currentContent = showOverlayFrame(content[index], e.which == 39);
		}

		e.preventDefault();
	});



}

function gallery(event) {
	var container = $(event.target);
	var startImage = container;
	container = container.parent();

	var images = [];

	container.find("img").each(function () {
		images.push($(this).attr('src'));
	});


	function getImage(src) {
		return $("<img>").attr('src', src);
	}

	showOverlay(images.map(getImage), images.indexOf(startImage.attr('src')));
}