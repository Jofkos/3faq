<?php
session_start();
?>

<html>
  <head>
    <title>test</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
  </head>
  <body>
    <form>
      <input type="file" id="test" accept="image/*"><br>
      <div id="images">
      </div>
      <a onclick="doYourThing()">dooo</a>
    </form>
    
    <!--
    SELECT user.name AS Name, post.`text` AS Text FROM followees
      RIGHT JOIN posts post ON followed = post.author
      INNER JOIN users user ON user.id = post.author
    WHERE following = 0;
    -->
    
    <script type="text/javascript">
      function doYourThing() {
        var data = new FormData();    
        console.log($("#test"));
        data.append('file', $("#test")[0].files[0]);

        $.post({
          url: 'receive.php',
          data: data,
          processData: false,
          contentType: false,
          success: function(data) {
            var obj = JSON.parse(data);
            for (var i in obj) {
              $("<img src='" + obj[i] + "'>").appendTo($("#images"));
            }
            console.log(data);
          }
        });
      }
    </script>
  </body>
  
</html>