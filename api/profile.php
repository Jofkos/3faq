<?php
header("Content-Type: text/plain");

include_once("../post_management.php");
if (!$LOGGED_IN) die("Not logged in!");

if (!isset($_POST['field']) || !isset($_POST['value'])) {
	die("No field or value!!!");
}

$_POST['value'] = $db->real_escape_string($_POST['value']);
if ($_POST['field'] == "description") {
	$db->query("UPDATE profiles SET description = '$_POST[value]' WHERE id = $_USER[id]");
} elseif ($_POST['field'] == "displayname") {
	$db->query("UPDATE users SET displayname = '$_POST[value]' WHERE id = $_USER[id]");
} else {
	die("Invalid field!");
}

die($db->error);
