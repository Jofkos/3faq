<?php
//error_reporting(E_CORE_ERROR);
include($_SERVER['DOCUMENT_ROOT'] . "/ImageCropping.php");

header("Content-Type: application/json");
ob_start();


function error($err) {
	global $log;
	die(json_encode(
		array(
			"success" => false,
			"error" => $err,
			"post" => $_POST,
			"files" => $_FILES,
			"log" => $log,
			"php-errors:" => ob_get_clean()
		)
	));
}

include_once("../user_management.php");
if (!$LOGGED_IN) error("Not logged in!");

$result = array();
$failed = array();
$log = array();

$action = $_POST['action']; // can be 'upload' (file-only), 'store' (db), 'crop-n-set' (crop, set as pb)

if (!isset($action)) {
	error("Action not set!");
} else {
	$log[] = "action: $action";
}

if ($action == "crop-n-set") {
	if (count($_FILES) != 0)
		error("Setting pb has to be a crop action, no file should be provided!");

	$required_args = array('filename', 'offsetX', 'offsetY', 'width', 'height');
	foreach ($required_args as $required_arg) {
		if (!isset($_POST[$required_arg])) {
			error("$required_arg is not set!");
		}
	}

	if ($tmpPath = ImageCropping::cropImage("$_SERVER[DOCUMENT_ROOT]/$_POST[filename]", $_POST['offsetX'], $_POST['offsetY'], $_POST['width'], $_POST['height'])) {

		foreach ($tmpPath as $size => $path) {
			$_FILES[] = array(
				"tmp_name" => $path,
				"name" => "cropped.png",
				"type" => "image/png",
				"cropsize" => $size
			);
		}
	}

}

foreach ($_FILES as $file) {
	$log[] = "Dealing with $file[cropsize]...";
	if (substr($file['type'], 0, 5) !== "image") continue;
	$log[] = "got through";
	$filename = "uploads/" . md5_file($file['tmp_name']) . '.' . strtolower(pathinfo($file['name'])['extension']);

	$log[] = "calculated name for file $file[tmp_name]: '$filename'";
	if (file_exists($filename) && $action != 'upload') {
		$log[] = "action is not upload file file exists. Fetching id from db...";

		if ($query = $db->query("SELECT id FROM images WHERE path = '$filename'")) {
			$id = $query->fetch_object()->id;
		}
	} else {
		$log[] = "file does not exists";
		if ($db->query("INSERT INTO images (path) VALUES ('$filename')")) {
			$log[] = "either mode is upload, or the file was inserted into db and got an id: " . $file['cropsize'];

			if (isset($file['cropsize'])) {
				$id[$file['cropsize']] = $db->insert_id;
				$log[] = "set id[$file[cropsize]] to " . $db->insert_id;
			} else {
				$log[] = "clearing out $id";
				$id = $db->insert_id;
			}

			if (!move_uploaded_file($file['tmp_name'], "$_SERVER[DOCUMENT_ROOT]/$filename")) {
				$log[] = "uploaded file could not be moved.";
				rename($file['tmp_name'], "$_SERVER[DOCUMENT_ROOT]/$filename");
				$log[] = "renamed or not: ";
			}
		}
	}

	$result['filename'][] = $filename;
}


if (isset($id)) {
	$log[] = "id is set";
	if ($action == 'crop-n-set') {
		$log[] = "setting as pb";
		print_r($id);
		$log[] = "UPDATE profiles SET picture = $id[small], picture_big = $id[big] WHERE id = $_USER[id]";
		if (!$db->query("UPDATE profiles SET picture = $id[small], picture_big = $id[big] WHERE id = $_USER[id]")) {
			error($db->error);
		}
	}
} else {
	$log[] = "$filename failed, has no id";
	$failed[] = $filename;
}

$result['success'] = true;
$result['failed'] = $failed;

$result['post'] = $_POST;
$result['files'] = $_FILES;
$result['log'] = $log;


$result['id'] = $id;

if (count($result['filename']) == 1) {
	$result['filename'] = array_shift($result['filename']);
}

$result['php-errors'] = ob_get_clean();

echo json_encode($result);

