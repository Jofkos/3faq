$(function() {

	$("#profile-pic").click(function () {
		var imagePreview = $('#image-preview');
		imagePreview.show();
		showOverlay(imagePreview, 0, function () {
			imagePreview.hide();
			imagePreview.appendTo($(document.body));
		});
	});
	
	var followButton = $(".follow-button");
	var followState = followButton.attr('data-follows') == '1';

	function toggleFollowState() {
		followButton.attr('data-follows', followState ? '0' : '1');
		followState = !followState;
	}

	followButton.click(function() {
		toggleFollowState();

		$.post(
			'/api/follow.php', {
				target: userId,
				action: followState
			},
			function(data, status) {

				if (!data.success) {
					toggleFollowState();
				} else {
					$($(".stats").find("div")[1]).html(data.newFollowerCount + "<br><span>Follower</span>");
					$('.follow .follow-status').eq(1).html(followState ? "Du folgst" : "Du folgst nicht");
				}
			},
			"json"
		)
	});


	function registerEditableField(editButton, valueField, fieldName) {
		valueField.addClass("editable");
		editButton.click(function() {
			editButton.toggleClass('fa-edit');
			editButton.toggleClass('fa-save');

			if (valueField.attr("contenteditable") == "true") {
				valueField.attr("contenteditable", "false");

				valueField.html(valueField.html()); // remove auto-correction underlining

				$.post('/api/profile.php', {
					field: fieldName,
					value: valueField.html()
				});
			} else {
				valueField.attr("contenteditable", "true");
			}
		});
	}


	var descriptionEditButton = $("#edit-description");
	var description = $("#profile-details").find("tr:last-child td:nth-child(2)");

	registerEditableField(descriptionEditButton, description, 'description');

	var displayname = $("#displayname");
	registerEditableField($("#edit-displayname"), displayname, 'displayname');

	displayname.keydown(function(event) {
		if (event.which != 8 && displayname.text().length > 36) {
			event.preventDefault();
		}
	});

	var uploadButton = $("#upload-button");
	var profileChooser = $("#profile-chooser");

	profileChooser.change(function () {
		console.log("SELECT!");

		uploadButton.addClass('loading');

		$.post({
			url: '/api/profile-pic.php',
			data: new FormData(profileChooser.parent().get(0)),
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function (data) {
				console.log(data);

				uploadButton.off('click');

				$('#image-preview').appendTo($(document.body));

				var cropping = $('#cropping');
				var img = cropping.find('img');
				img.attr('src', data.filename);
				var saveButton = cropping.find('div:last-child');

				cropping.show();
				if (hideModal !== undefined)
					hideModal();

				showOverlay(cropping, 0, function () {
					uploadButton.removeClass("loading");
					uploadButton.on('click', selectFile);

					img.off('load');
					saveButton.off('click');
					cropping.appendTo($(document.body));

					unbindCropping();
				});
				cropping.parent().addClass('cropping-content');

				img.on('load', function () {
					bindCropping();
					saveButton.on('click', saveToFile);
				});
			}
		});
	});

	function selectFile(event) {
		event.stopPropagation();
		event.preventDefault();
		console.log("no prop and def");


		var data = new FormData();
		data.append('file', profileChooser[0].files[0]);
		console.log('click!');


		profileChooser.click();

	}

	uploadButton.on('click', selectFile);
});