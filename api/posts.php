<?php
header("Content-Type: text/json");
include_once("../post_management.php");

$olderThan = "NOW()";
$newerThan = "0";

if (isset($_POST['olderThan'])) {
	$olderThan = "FROM_UNIXTIME(" . $_POST['olderThan'] . ")";
}

if (isset($_POST['newerThan'])) {
  $newerThan = "FROM_UNIXTIME(" . $_POST['newerThan'] . ")";
}

if (isset($_POST['from'])) {
	echo json_encode(getPostsFrom($_POST['from'], $olderThan, $newerThan, true));
} else {
	echo json_encode(getPosts($olderThan, $newerThan, true));
}
