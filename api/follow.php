<?php
header("Content-Type: application/json");

function error($err) {
	die(json_encode(
		array(
			"success" => false,
			"error" => $err
		)
	));
}

include_once("../post_management.php");

if (!$LOGGED_IN) error("Not logged in!");

if (!isset($_POST['target']) || !isset($_POST['action'])) {
	error("Not all fields set!");
}

$target = intval($_POST['target']); // targetId
$action = $_POST['action'] == "true"; // true = follow, false = unfollow

$result = array();

$success = true;

if ($action) {
	$success = $db->query("INSERT INTO followees (user, following) VALUES ($_USER[id], $target)");
} else {
	$success = $db->query("DELETE FROM followees WHERE user = $_USER[id] AND following = $target");
}

$result['success'] = $success;

if ($success) {
	$result['newFollowerCount'] = $db->query("SELECT COUNT(*) AS followers FROM followees WHERE following = $target")->fetch_object()->followers;
}

echo json_encode($result);