<?php

$postId = $data;

$posts = getPostsAndResponses($postId);
if (!$posts || count($posts) == 0) {
	?>
  <div class="column">
    <h1>Dieser Post existiert nicht.</h1></div>
  <?php
	die();
}

$username = $posts[0]->username;
$theUser = getUserInformation($username);

$thePost = array_pop($posts);

?>
    <div class="column">
      <div class="box">
	      <?php echo $thePost->html; ?>
      </div>
      <?php

      if ($LOGGED_IN) {
	      ?>
          <div class="box">
              <div class="row">
                  <h1>Antworten!</h1>
                  <div class="left">
                      <img class="profile_pic post" src="<?php echo "/$_USER[picture]"; ?>">
                  </div>
                  <div class="right">
                      <form enctype="multipart/form-data" action="/post_handling.php" method="POST" id="post-form">


                          <textarea name="text" required></textarea>
                          <input accept="image/*" type="file" multiple="multiple" name="images[]">

                          <input type="hidden" name="response" value="<?php echo $thePost->id; ?>">

                          <div>
                              <div id="add-images">
                                  <i class="fa fa-file-image-o fa-2" aria-hidden="true"> Bilder hinzufügen</i>
                              </div>
                              <button value="Abschicken" name="Abschicken">Abschicken</button>
                          </div>
                          <div id="choosen-pictures"></div>
                      </form>
                  </div>
              </div>
          </div>
	      <?php
      }
      ?>

        <div class="box">
		    <?php
		    foreach ($posts as $post) {
			    echo $post->html;
		    }
		    ?>
        </div>
    </div>

    <script type="text/javascript">
      $(function() {
        var newestPost = <?php echo count($posts) > 0 ? $posts[0]->timestamp : 0; ?>;
        var oldestPost = <?php echo count($posts) > 0 ? $posts[count($posts) - 1]->timestamp : 0; ?>;


        $(window).on('scrollReachedBottom', function() {
          return; // TODO
          setLoading(true);

          $.post(
            '/api/posts.json', {
              olderThan: oldestPost,
              from: '<?php echo $username; ?>'
            },
            function(data, status) {
              var posts = $("#posts");
              $.each(data, function(index, value) {
                buildPost(value).appendTo(posts);
                oldestPost = value.timestamp;
              });
              setLoading(false);
            },
            "json"
          );
        });
      });
    </script>