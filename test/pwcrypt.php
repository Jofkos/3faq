<form method="POST">
	<input type="text" name="password">
	<input type="submit">
</form>


<?php

function hashPassword($password) {
	return password_hash($password, PASSWORD_BCRYPT);
}

if (isset($_POST['password'])) {
	echo hashPassword($_POST['password']);
}
