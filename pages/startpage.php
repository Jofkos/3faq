<div class="column">
	<?php
	if (!$LOGGED_IN) {
		?>
        <div class="box">
            <h1>Registrieren</h1>
            <div>
                <?php
                if (isset($_GET['register'])) {
                    echo "Du bist registriert und kannst dich nun einloggen!";
                }
                ?>
                <form id="register" method="post" action="register.php">
                    <input type="text" id="username" name="username" placeholder="Benutzername" required>
                    
                    <input type="text" name="displayname" placeholder="Anzeigename" required>
                    
                    <input type="text" id="birthday" name="birthday" placeholder="Geburtstag (dd.mm.yyyy)" required>
                    
                    <select name="gender">
                        <option value="2" selected>Keine Angabe</option>
                        <option value="0">Männlich</option>
                        <option value="1">Weiblich</option>
                    </select>
                    <input type="password" name="password" placeholder="Passwort" required>
                    <button type="submit"><i class="fa fa-plus-circle" aria-hidden="true"> Registrieren!</i></button>
                </form>
            </div>
        </div>


	<?php }

	if ($LOGGED_IN) {
		?>
        <div class="box">
            <div class="row">
                <h1>Neuen Post erstellen</h1>
                <div class="left">
                    <img class="profile_pic post" src="<?php echo $_USER['picture']; ?>">
                </div>
                <div class="right">
                    <form enctype="multipart/form-data" action="post_handling.php" method="POST" id="post-form">


                        <textarea name="text" required></textarea>
                        <input accept="image/*" type="file" multiple="multiple" name="images[]">

                        <div>
                            <div id="add-images">
                                <i class="fa fa-file-image-o fa-2" aria-hidden="true"> Bilder hinzufügen</i>
                            </div>
                            <button value="Abschicken" name="Abschicken">Abschicken</button>
                        </div>
                        <div id="choosen-pictures"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="box" id="posts">
			<?php

			$posts = getPosts();

			if (count($posts) == 0) {
				echo "Keine Beiträge gefunden!";
			} else {
				foreach ($posts as $post) {
					echo $post->html;
				}
			}
			?>
        </div>
	<?php } ?>
</div>

<script type="text/javascript">

	$(window).on('scriptJsLoaded', function () {
			var oldestPost = <?php echo count($posts) > 0 ? $posts[count($posts) - 1]->timestamp : 0; ?>;

			registerInfiniteScroll(oldestPost, $("#posts"), {});
		}
	);
</script>