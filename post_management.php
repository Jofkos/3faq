<?php
error_reporting(E_CORE_ERROR);
require_once("user_management.php");

function getPosts($olderThan = "NOW()", $newerThan = "0", $new = false, $limit = 6) {
	global $_USER;

	return getPostsFromQuery("
      SELECT
        post.id,
        author.id as author_id,
        author.username,
        author.displayname,
        author_profile_pic.path as profile_picture,
        post.at,
        post.text,
        GROUP_CONCAT(image.path) AS images,
        COALESCE(likes.likeCount, 0) as likes,
        (SELECT userLiked.user IS NOT NULL) AS userliked
      FROM followees followee
    
        RIGHT JOIN posts post ON post.author = followee.following
        LEFT JOIN (SELECT post, COALESCE(COUNT(*), 0) as likeCount FROM likes GROUP BY post) as likes ON likes.post = post.id
        LEFT JOIN (SELECT * FROM likes) AS userLiked ON userLiked.user = followee.user AND userLiked.post = post.id
        INNER JOIN users author ON author.id = post.author
        INNER JOIN profiles author_profile ON author.id = author_profile.id
        INNER JOIN images author_profile_pic ON author_profile.picture = author_profile_pic.id
        LEFT JOIN post_images post_image ON post_image.post = post.id
        LEFT JOIN images image ON post_image.image = image.id
        
      WHERE
        (followee.user = $_USER[id] OR author.id = $_USER[id])
        AND post.at > $newerThan
        AND post.at < $olderThan
				AND post.response IS NULL
      GROUP BY post.id
      ORDER BY post.at DESC
      LIMIT $limit;", $new);
}

function getPostsFrom($author, $olderThan = "NOW()", $newerThan = "0", $new = false, $limit = 6) {
  global $_USER, $LOGGED_IN;

	return getPostsFromQuery("
      SELECT
        post.id,
        author.id as author_id,
        author.username,
        author.displayname,
        author_profile_pic.path as profile_picture,
        post.at,
        post.text,
        GROUP_CONCAT(image.path) AS images,
        COALESCE(likes.likeCount, 0) as likes" . ($LOGGED_IN ? ",
        (SELECT userLiked.user IS NOT NULL) AS userliked" : "") . "
      FROM posts post
      
        LEFT JOIN (SELECT post, COUNT(*) as likeCount FROM likes GROUP BY post) as likes ON likes.post = post.id
        " . ($LOGGED_IN ? "LEFT JOIN (SELECT * FROM likes) AS userLiked ON userLiked.user = $_USER[id] AND userLiked.post = post.id" : "") . "
        INNER JOIN users author ON author.id = post.author
        INNER JOIN profiles author_profile ON author.id = author_profile.id
        INNER JOIN images author_profile_pic ON author_profile.picture = author_profile_pic.id
        LEFT JOIN post_images post_image ON post_image.post = post.id
        LEFT JOIN images image ON post_image.image = image.id
    
      WHERE
        author.username = '$author'
        AND post.at > $newerThan
        AND post.at < $olderThan
				AND post.response IS NULL
      GROUP BY post.id
      ORDER BY post.at DESC
      LIMIT $limit;", $new);
}

function getPostsAndResponses($id, $olderThan = "NOW()", $newerThan = "0", $new = false, $limit = 6) {
  global $_USER, $LOGGED_IN;

	return getPostsFromQuery("
      SELECT
        post.id,
        author.id as author_id,
        author.username,
        author.displayname,
        author_profile_pic.path as profile_picture,
        post.at,
        post.text,
        GROUP_CONCAT(image.path) AS images,
        COALESCE(likes.likeCount, 0) as likes" . ($LOGGED_IN ? ",
        (SELECT userLiked.user IS NOT NULL) AS userliked" : "") . "
      FROM posts post
      
        LEFT JOIN (SELECT post, COUNT(*) as likeCount FROM likes GROUP BY post) as likes ON likes.post = post.id
        " . ($LOGGED_IN ? "LEFT JOIN (SELECT * FROM likes) AS userLiked ON userLiked.user = $_USER[id] AND userLiked.post = post.id" : "") . "
        INNER JOIN users author ON author.id = post.author
        INNER JOIN profiles author_profile ON author.id = author_profile.id
        INNER JOIN images author_profile_pic ON author_profile.picture = author_profile_pic.id
        LEFT JOIN post_images post_image ON post_image.post = post.id
        LEFT JOIN images image ON post_image.image = image.id
    
      WHERE
        (post.id = $id OR post.response = $id)
        AND post.at > $newerThan
        AND post.at < $olderThan
      GROUP BY post.id
      ORDER BY post.at DESC
      LIMIT $limit;", $new);
}

function getPostsFromQuery($query, $new = false) {
    global $db;
//    echo $query;
    $query = $db->query($query);

    $posts = array();

	while ($post = $query->fetch_object()) {
		$postTime = new DateTime($post->at);
		$interval = $postTime->diff(new DateTime('now'));

		if ($interval->days > 1) {
			$timeString = $postTime->format("\a\m d.m.Y \u\m H:i:s");
		} else if ($interval->days == 1) {
			$timeString = "gestern um " . $postTime->format("H:i:s");
		} else {
			if ($interval->h <= 1) {
				if ($interval->i <= 1) {
					$timeString = $interval->format('vor %s Sekunden');
				} else {
					$timeString = $interval->format('vor %i Minuten');
				}
			} else {
				$timeString = $interval->format('vor %h Stunden');
			}
		}

		$post->atString = $timeString;
		$post->timestamp = $postTime->getTimestamp();
		$post->html = getPostHTML($post, $new);
		$posts[] = $post;
	}

//	echo json_encode($posts);

	return $posts;
}


//function insertPost($post, $new = false) {
//	echo getPostHTML($post, $new);
//}

function getPostHTML($post, $new = false) {
    $classes = "row post" . ($new ? " new" : "");
    $iconClass = "fa-heart" . (!$post->userliked ? "-o" : "");

    $imgContainer = "";

    if (!empty($post->images)) {
    	$imgContainer .= '<div class="imgcontainer">';

    	$images = explode(',', $post->images);
    	foreach ($images as $image) {
    		if (substr($image, 0, 1) != '/') {
    			$image = "/$image";
		    }
    		$imgContainer .= "<img src='$image'>";
	    }

    	$imgContainer .= '</div>';
    }

	return <<<HTML
    <div class="$classes" onclick="goTo('/post/$post->id')">
        <div class="left">
            <img src="/$post->profile_picture" class="profile_pic post">
        </div>
        <div class="right">
            <span onclick="goTo('/$post->username')">$post->displayname <small>@$post->username</small></span>
            <br>
            <div>$post->text</div>
            $imgContainer
            <div class="footer">
                <i class="fa $iconClass fa-2" aria-hidden="true" onclick="like(this, $post->id)"> $post->likes</i>
                <div class="date">$post->atString</div>
            </div>
        </div>
    </div>
HTML;
}