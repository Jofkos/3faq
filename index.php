<?php
error_reporting(E_CORE_WARNING);
session_start();

if (isset($_COOKIE['sid'])) {
	$_SESSION['sid'] = $_COOKIE['sid'];
}

include_once("user_management.php");
include_once("post_management.php");

$category = $_GET['category'];
$data = $_GET['data'];

?>
<!DOCTYPE html>
<html>
<head>
    <title>3faq</title>

    <meta charset="utf8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- jQuery - lightweight javascript library | https://jquery.com/ -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <!-- Font Awesome | Icons | http://fontawesome.io/ -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Font -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Vollkorn">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu">

    <!-- Style und Javascript -->
    <link href="/style.css" rel="stylesheet">
    <script type="text/javascript">
        $(function() {
	        $.getScript('/js/overlay.js').done(function() {
		        $.getScript('/js/script.js');
            });
        });
    </script>
</head>
<body>
<div id="header">
    <div class="centered-content">
        <div id="home-button" onclick="goTo('/')"></div>
        <h1 id="title">3f<span>a</span>q <span id="status"><?php
				if ($LOGGED_IN) {
					?>
                    <img src="/<?php echo $_USER['picture']; ?>" class="profile_pic header">
					<?php
				} else {
					?>
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
					<?php
				}
				?>
					</span></h1>
    </div>
</div>

<div id="overlay">
    <i class="fa fa-times" aria-hidden="true"></i>
    <div id="overlay-click"></div>
</div>

<div class="hidden top-caret" id="top-popup">
	<?php if ($LOGGED_IN) { ?>
        <div class="row clickable">
            <?php echo "<span onclick=\"goTo('/$_USER[username]')\">$_USER[displayname]<br><small>Profil anzeigen</small></span>"; ?>
        </div>
        <div class="row clickable" id="logout">
            Ausloggen
        </div>

	<?php } else { ?>
        <form method="POST">
            <input type="text" name="username" placeholder="Benutzername">
            <input type="password" name="password" placeholder="Passwort">
            <button type="submit">
                <i class="fa fa-sign-in" aria-hidden="true"> Einloggen</i>
            </button>
        </form>
	<?php } ?>
</div>

<div id="content" class="centered-content">
    <?php

    if ($category == "profile" && isset($data)) {
        include($_SERVER['DOCUMENT_ROOT'] . "/pages/profile.php");
    } else if ($category == "post" && isset($data)) {
	    include($_SERVER['DOCUMENT_ROOT'] . "/pages/post.php");
    } else {
	    include($_SERVER['DOCUMENT_ROOT'] . "/pages/startpage.php");
    }

    ?>

</div>

</body>
</html>