<?php
require_once("mysql.php");

/*    SESSION STUFF    */
session_start();

$_USER = array();

if (isset($_POST['username']) && isset($_POST['password']) && !isset($_POST['register'])) {

	if ($id = verifyLoginPassword($_POST['username'], $_POST['password'])) {
		$_SESSION['sid'] = bin2hex(openssl_random_pseudo_bytes(18));
		$db->query("INSERT INTO sessions VALUES('$_SESSION[sid]', $id)");
	}
}

if (isset($_SESSION['sid'])) {
	setcookie('sid', $_SESSION['sid'], time() + 60 * 60 * 24 * 30, '/'); // cookie erneuern

	$query = $db->query("
	  SELECT
	    user.id as id,
	    user.username,
	    user.displayname,
	    profile.birthday,
	    profile.gender,
	    profile.description,
	    picture.path as picture,
	    picture_big.path as picture_big
	  FROM sessions session
	
	  INNER JOIN users user ON session.user = user.id
	  INNER JOIN profiles profile ON user.id = profile.id
	  LEFT JOIN images picture ON picture.id = profile.picture
	  LEFT JOIN images picture_big ON picture_big.id = profile.picture_big
	
	  WHERE session.id = '" . $db->real_escape_string($_SESSION['sid']) . "'");
	if ($query && $result = $query->fetch_assoc()) {
		$_USER = $result;
	}
}

$LOGGED_IN = count($_USER) > 0;

function getUserInformation($nameOrId) {
	global $db;
	$nameOrId = $db->real_escape_string($nameOrId);
	if (!is_numeric($nameOrId)) {
		$nameOrId = "'$nameOrId'";
	}

	$query = $db->query("
	  SELECT
	    user.id as id,
	    user.username,
	    user.displayname,
	    DATE_FORMAT(profile.birthday, '%m.%d.%Y') as birthday,
	    profile.gender,
	    profile.description,
	    picture.path as picture,
	    picture_big.path as picture_big
	  FROM users user
	
	  INNER JOIN profiles profile ON user.id = profile.id
	  LEFT JOIN images picture ON picture.id = profile.picture
	  LEFT JOIN images picture_big ON picture_big.id = profile.picture_big
	
	  WHERE user.id = $nameOrId OR user.username = $nameOrId");
	if ($query && $result = $query->fetch_assoc()) {
		return $result;
	}
	return false;
}

function getUserStats($username) {
	global $db, $LOGGED_IN, $_USER;
	$stats = array(
		"posts" => 0,
		"followers" => 0,
		"following" => 0
	);

	if ($result = $db->query("SELECT COUNT(*) as posts FROM posts post JOIN users author ON author.id = post.author WHERE author.username = '$username' AND post.response IS NULL")->fetch_object()) {
		$stats['posts'] = $result->posts;
	}

	if ($result = $db->query("SELECT COUNT(*) AS following FROM followees followee JOIN users user ON user.id = followee.following WHERE user.username = '$username'")->fetch_object()) {
		$stats['following'] = $result->following;
	}

	if ($result = $db->query("SELECT COUNT(*) AS followers FROM followees followee JOIN users user ON user.id = followee.user WHERE user.username = '$username'")->fetch_object()) {
		$stats['followers'] = $result->followers;
	}

	if ($LOGGED_IN) {
		if ($result = $db->query("SELECT EXISTS(SELECT * FROM followees followee JOIN users follower ON follower.id = followee.user WHERE follower.username = '$username' AND followee.following = $_USER[id]) AS is_followed_by;")->fetch_object()) {
			$stats['is_followed_by'] = $result->is_followed_by == 1;
		}

		if ($result = $db->query("SELECT EXISTS(SELECT * FROM followees followee JOIN users followed ON followed.id = followee.following WHERE followed.username = '$username' AND followee.user = $_USER[id]) AS follows;")->fetch_object()) {
			$stats['follows'] = $result->follows == 1;
		}
	}

	return $stats;
}

/*    PASSWORD STUFF    */
function hashPassword($password) {
	return password_hash($password, PASSWORD_BCRYPT);
}

function verifyPassword($input, $hashedPassword) {
	return password_verify($input, $hashedPassword);
}

function verifyLoginPassword($user, $passwordInput) {
	global $db;
	$user = $db->real_escape_string($user);
	if ($result = $db->query("SELECT id, password FROM users WHERE username = '$user'")->fetch_object()) {
		if (verifyPassword($passwordInput, $result->password)) {
			return $result->id;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function storeUser($username, $name, $password) {
	global $db;
	$statement = $db->prepare("INSERT INTO users (username, displayname, password) VALUES (?, ?, ?)");
	$statement->bind_param("sss", $username, $name, hashPassword($password));
	if ($statement->execute()) {
		if ($db->query("INSERT INTO profiles (id) VALUES($statement->insert_id)")) {
			return true;
		}
	}
	$statement->close();
}